---
layout: page
title: About
permalink: /about/
---

Back in 2013, Mark Carew of OpenBuilds
[created](https://openbuilds.com/threads/openbuilds-ox-cnc-machine.134/)
the [OX CNC router](https://openbuilds.com/builds/openbuilds-ox-cnc-machine.341/)
as a variant of the ROUTY, itself a derivative of Shapeoko.

The SMW3D OX CNC router variants were then created and sold by
SMW3D / Hobby-Fab over several years, as relatively large-format
derivatives of the original OX design.

This site is intended to collect information useful to build and
maintain OX CNC machines, particularly the SMW3D OX CNC variants and
its derivatives.  This includes information released by Hobby-Fab
when they closed operations, as well as information from the original
OX build from OpenBuilds.

Discuss at [MakerForums](https://forum.makerforums.info/c/cnc-routers/ox)
and there, tag [@mcdanlj](https://forum.makerforums.info/u/mcdanlj)
for updates to ths site, or clone [this repository at GitLab](https://gitlab.com/mcdanlj/smw3d-ox),
make your suggested changes, and create a merge request.
