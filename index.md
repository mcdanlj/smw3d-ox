---
layout: home
---

This site is [about](/about/) the now-defunct
SWM3D / Hobby-Fab variant of the [OpenBuilds OX](https://openbuilds.com/builds/openbuilds-ox-cnc-machine.341/).
[SWM3D offered](https://web.archive.org/web/20180908140013/https://www.hobby-fab.com/ox-cnc-router)
three sizes:
* 500mm x 750mm
* 750mm x 1000mm
* 1500mm x 1500mm

SWM3D offered this comparison chart:
![Comparison chart](/assets/images/OX_comparison_chart.jpg)

The [original OpenBuilds OX build](https://openbuilds.com/builds/openbuilds-ox-cnc-machine.341/)
is the base source of documentation, and has links to all the requires OpenBuilds parts.
This site has additional information and specifics for the kits sold by SWM3D / Hobby-Fab.

(Information on the [R7](https://github.com/eclsnowman/R7-CNC) router
is separately available on GitHub.)

# Forum

[Maker Forums](https://forum.makerforums.info/) has an
[OX CNC](https://forum.makerforums.info/c/cnc-routers/ox) 
category. The [OpenBuilds OX build](https://openbuilds.com/builds/openbuilds-ox-cnc-machine.341/)
contains a forum under the "Discussions" tab.

# Build Instructions

SMW3D published several sets of build instructions. Here are some originals:

* Small Format with photographs (revised order of build steps)
  * [PDF](/assets/documents/Small_Format_OX_Build_instruction_rev_5.pdf)
  * [DOCX](/assets/documents/Small_Format_OX_Build_instruction_rev_5.docx)
* Large Format with photographs (revised order of build steps)
  * [PDF](/assets/documents/Large_Format_OX_instructions_v4.2.pdf)
  * [DOCX](/assets/documents/Large_Format_OX_instructions_v4.2.docx)
* Modified version of OpenBuilds Z axis build instructions, with models
  * [PDF](/assets/documents/OX_Z_Build_Instructions.pdf)
  * [DOCX](/assets/documents/OX_Z_Build_Instructions.docx)

The [OpenBuilds OX build](https://openbuilds.com/builds/openbuilds-ox-cnc-machine.341/)
has build videos, electronics suggestions, and even sticker packs if you want to
decorate your router with OpenBuilds OX logos.

# Build Assets

[More CAD files are coming in SolidWorks and open formats at some time in the
future.](https://forum.makerforums.info/t/making-an-smw3d-ox-site-volunteers/77395)

## Plates

* Version 3:
  * [IGS](/assets/plates/OX_Plates_R3.IGS)
  * [DXF](/assets/plates/OX_Plates_R3.DXF)
  * [CamBam](/assets/plates/OX_Plates_R3.cb)
  * [GCODE](/assets/plates/OX_Plates_R3.gcode), [NC](/assets/plates/OX_Plates_R3.nc)
  * Solidworks
    * [plates SLDPRT](/assets/plates/OX_Plates_R3.SLDPRT)
    * [Rear gantry plate](/assets/plates/OX_Rear_gantry_plate.SLDPRT)
    * [Gantry plates parts](/assets/plates/Set_of_OX_gantry_plates.SLDPRT)
    * [Gantry plates parts](/assets/plates/Set_of_OX_gantry_plates_part.SLDPRT)
    * [Gantry plates assembly](/assets/plates/Set_of_OX_gantry_plates.SLDASM)

* Original
  * [DXF](/assets/plates/Full_Set_3.5_for_FO_w_Probe.dxf)
  * [GCODE (set of two)](/assets/plates/Full_set_3.5_for_FO_x_2.gcode)
  * [CADfile](/assets/plates/Full_Set_3.5_for_FO_w_Probe.cb)

## Parts

  * [Stronger Z assembly](/assets/plates/Stronger_Z_assembly.SLDASM)
  * [Drop-in Tee-Nut part](/assets/plates/Drop-_in_Tee-Nut.SLDPRT)
